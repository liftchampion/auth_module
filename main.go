package main

import (
	"../github.com/dgrijalva/jwt-go"
	"bytes"
	"fmt"
	"strconv"
	"time"
)

const access_token_validity_time time.Duration = time.Minute * 5
var signing_key = []byte("azino777")
var count_refresh_tokens = 0


func validate_user(username string, password string) (string, bool, error){

	// Send request to database. If true => return true.
	// If error => return error.

	//user_id, res, err := send_request(username, password)
	//err := fmt.Errorf("user %q (id %d) not found", name, id)


	if username != "invalid" && password != "invalid" {
		return username, true, nil
	} else {
		err := fmt.Errorf("403")
		return "BAD", false, err
	}
	err := fmt.Errorf("666")
	return "BAD", false, err
}

func get_user_groups(username string) ([]int, error) {
	// Send request to database.
	// If error => return error.

	groups_id := []int{1, 3, 3, 7,}
	return groups_id, nil
}

func validate_and_get_user_groups(username string, password string) (string, bool, []int, error){
	var err error
	var groups_id []int
	var validation_res bool
	var user_id string

	user_id, validation_res, err = validate_user(username, password)
	if validation_res {
		groups_id, err = get_user_groups(username)
	}
	return user_id, validation_res, groups_id, err
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

func print_tokens(tokens Tokens){
	access, err_a := parse_access_token(tokens.access_token)
	refresh, err_r := parse_refresh_token(tokens.refresh_token)

	if err_a != nil {
		fmt.Println("invalid access token")
	} else {
		fmt.Printf("%v %v %v\n", access.StandardClaims.IssuedAt, access.StandardClaims.ExpiresAt, access.GroupsId)
	}
	if err_r != nil {
		fmt.Println("invalid refresh token")
	} else {
		fmt.Printf("%v %v %v %v\n", refresh.StandardClaims.IssuedAt, refresh.GroupsId, refresh.UserId, refresh.TokenId)
	}
}

func parse_access_token(token_str string) (AccessClaims, error){
	token, err := jwt.ParseWithClaims(token_str, &AccessClaims{}, func(token *jwt.Token) (interface{}, error) {
		return signing_key, nil
	})
	if err != nil {
		return AccessClaims{}, err
	}

	claims, ok := token.Claims.(*AccessClaims)
	if !(ok && token.Valid) {
		return AccessClaims{}, err
	}
	return *claims, nil
}

func parse_refresh_token(token_str string) (RefreshClaims, error){  // ADD DATABASE CHECK (unique id) + add another error code
	token, err := jwt.ParseWithClaims(token_str, &RefreshClaims{}, func(token *jwt.Token) (interface{}, error) {
		return signing_key, nil
	})
	if err != nil {
		return RefreshClaims{}, fmt.Errorf("403")
	}

	claims, ok := token.Claims.(*RefreshClaims)
	if !(ok && token.Valid) {
		return RefreshClaims{}, fmt.Errorf("403")
	}
	ok, err = IsValidTokenId(claims.UserId, claims.TokenId)
	if  err != nil{
		return RefreshClaims{}, fmt.Errorf("redis error")
	}
	if !ok {
		return RefreshClaims{}, fmt.Errorf("423")
	}
	return *claims, nil
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

func array_to_str(arr []int) string {
	var buf bytes.Buffer

	buf.WriteString("[ ")
	for idx, item := range arr{
		buf.WriteString(strconv.Itoa(item))
		if idx != len(arr) - 1{
			buf.WriteString(",")
		}
		buf.WriteString(" ")
	}
	buf.WriteString("]")
	return buf.String()
}

type Tokens struct {
	access_token string
	refresh_token string
}

type AccessClaims struct {
	GroupsId[] int `json:"groups_id"`
	jwt.StandardClaims
}

type RefreshClaims struct {
	GroupsId[] int `json:"groups_id"`
	UserId string `json:"user_id"`
	TokenId string `json:"token_id"`
	jwt.StandardClaims
}

func make_token_id() string {
	var buf bytes.Buffer

	buf.WriteString(strconv.FormatInt(time.Now().Unix(), 10))
	buf.WriteString(".")
	buf.WriteString(strconv.FormatInt(time.Now().UnixNano() % int64(time.Millisecond), 10))
	buf.WriteString(".")
	buf.WriteString(strconv.Itoa(count_refresh_tokens))
	count_refresh_tokens++
	return buf.String()
}

func make_token(claims jwt.Claims) (string, error){
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	token_str, err := token.SignedString(signing_key)
	if err != nil {
		return "ERROR", fmt.Errorf("token making jwt error")
	}
	return token_str, nil
}

func CreateRefreshToken(user_id string, groups_id[] int) (string, string, error){
	var claims jwt.Claims
	token_id := make_token_id()

	claims = RefreshClaims{
		groups_id,
		user_id,
		token_id,
		jwt.StandardClaims{
			IssuedAt:  time.Now().Unix(),
		},
	}
	token_str, err := make_token(claims)

	return token_str, token_id, err
}

func CreateAccessToken(groups_id[] int) (string, error){
	var claims jwt.Claims

	claims = AccessClaims{
		groups_id,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(access_token_validity_time).Unix(),
			IssuedAt:  time.Now().Unix(),
		},
	}

	token_str, err := make_token(claims)

	return token_str, err
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

func make_and_reg_tokens_pair(user_id string, groups_id[] int, mode string, old_token_id string) (Tokens, error) {
	var tokens Tokens
	var err error
	var token_id string

	tokens.access_token, err = CreateAccessToken(groups_id)
	if err != nil{
		return Tokens{}, err
	}
	tokens.refresh_token, token_id, err = CreateRefreshToken(user_id, groups_id)
	if err != nil{
		return Tokens{}, err
	}
	if mode == "ADD"{
		err = AddTokenIdToUser(user_id, token_id)
	}
	if mode == "UPDATE"{
		err = UpdateTokenId(user_id, old_token_id, token_id)
	}
	if err != nil{
		return Tokens{}, err
	}
	return tokens, nil
}

func login(username string, password string) (Tokens, error) {
	var err error
	var groups_id []int
	var validation_res bool
	var tokens Tokens
	var user_id string

	user_id, validation_res, groups_id, err = validate_and_get_user_groups(username, password)
	if err != nil {
		return Tokens{}, err
	}
	if validation_res{
		tokens, err = make_and_reg_tokens_pair(user_id, groups_id, "ADD", "adding")
		if err != nil{
			return Tokens{}, err
		}
	}
	return tokens, nil
}

func is_auth(access_token string) error {
	_, err := parse_access_token(access_token)
	if err != nil {
		return fmt.Errorf("401")
	} else {
		return nil
	}
}

func refresh_tokens(refresh_token string) (Tokens, error) {
	claims, err := parse_refresh_token(refresh_token)
	if err != nil {
		return Tokens{}, err
	}

	var tokens Tokens
	tokens, err = make_and_reg_tokens_pair(claims.UserId, claims.GroupsId, "UPDATE", claims.TokenId)
	if err != nil{
		return Tokens{}, err
	}

	return tokens, nil
}

func logout(refresh_token string) error{
	claims, err := parse_refresh_token(refresh_token)
	if err != nil {
		if err == fmt.Errorf("423"){
			return err
		}
		return fmt.Errorf("403")
	}
	err = RemoveTokenId(claims.UserId, claims.TokenId)
	if err != nil{
		return err
	}
	return nil
}

func force_logout_from_all_account_sessions(user_id string) error{
	err := RemoveAllValue(user_id)
	return err
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

func AddTokenIdToUser(UserId string, TokenId string) error{
	err := SetValue(UserId, TokenId)
	if err != nil{
		return err
	}
	return nil
}

func RemoveTokenId(UserId string, TokenId string) error{
	err := RemoveValue(UserId, TokenId)
	if err != nil{
		return err
	}
	return nil
}

func UpdateTokenId(UserId string, OldTokenId string, TokenId string) error{
	err := UpdateValue(UserId, OldTokenId, TokenId)
	if err != nil{
		return err
	}
	return nil
}

func IsValidTokenId(UserId string, TokenId string) (bool, error){
	is_in, err := HasValue(UserId, TokenId)
	if err != nil{
		return false, err
	}
	if is_in {
		return true, nil
	} else {
		return false, nil
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var storage = make(map[string][]string)

func  FindItemInArray(arr[] string, to_find string) (int, error){
	res := -1
	for idx, value := range arr{
		if value == to_find{
			res = idx
			break
		}
	}
	return res, nil
}

func SetValue(key string, value string) error{
	storage[key] = append(storage[key], value)
	return nil
}

func RemoveValue(key string, value string) error{
	idx, err := FindItemInArray(storage[key], value)
	if err != nil{
		return fmt.Errorf("0")
	}
	if idx == -1 {
		return nil
	}
	storage[key] = append(storage[key][:idx], storage[key][idx + 1:]...)
	if len(storage[key]) == 0{
		delete(storage, key)
	}
	return nil
}

func UpdateValue(key string, OldValue string, value string) error{
	err := RemoveValue(key, OldValue)
	if err != nil {
		return err
	}
	err = SetValue(key, value)
	if err != nil {
		return err
	}
	return nil
}

func RemoveAllValue(key string) error{
	delete(storage, key)
	return nil
}

func HasValue(key string, value string) (bool, error){
	pos, err := FindItemInArray(storage[key], value)
	if err != nil{
		return false, err
	}
	if  pos != -1{
		return true, nil
	} else {
		return false, nil
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

func main() {

	/*fmt.Println(storage)
	err := SetValue("1", "101")
	err = SetValue("1", "102")
	err = SetValue("1", "103")
	err = SetValue("1", "101")
	err = SetValue("2", "201")
	err = SetValue("2", "202")
	err = SetValue("3", "301")
	fmt.Println(storage)
	err = RemoveValue("1", "202")
	err = RemoveValue("1", "101")
	err = RemoveValue("3", "301")
	fmt.Println(storage)
	err = RemoveAllValue("2")
	err = UpdateValue("1", "101", "104")
	err = UpdateValue("1", "108", "105")
	fmt.Println(storage)
	fmt.Println(FindItemInArray(storage["1"], "101"))
	fmt.Println(FindItemInArray(storage["1"], "102"))
	fmt.Println(FindItemInArray(storage["10"], "101"))
	fmt.Println(storage)
	fmt.Println(err)*/

	start2 := time.Now()
	tokens, err := login("user", "password")
	if err == nil {
		//fmt.Println(tokens.access_token)
		//fmt.Println(tokens.refresh_token)
		//print_tokens(tokens)
	} else {
		fmt.Println("login error")
	}
	/*tokens, err = login("user", "password")
	if err == nil {
		fmt.Println(tokens.access_token)
		fmt.Println(tokens.refresh_token)
		print_tokens(tokens)
	} else {
		fmt.Println("login error")
	}
	tokens2, err2 := login("user2", "password")
	if err2 == nil {
		fmt.Println(tokens2.access_token)
		fmt.Println(tokens2.refresh_token)
		print_tokens(tokens2)
	} else {
		fmt.Println("login error")
	}*/

	fmt.Printf("\n\n")
	tokens, err = refresh_tokens(tokens.refresh_token)
	if err == nil {
		//fmt.Println(tokens.access_token)
		//fmt.Println(tokens.refresh_token)
		//print_tokens(tokens)
	} else {
		fmt.Println("login error")
	}
	//fmt.Printf("\n\n")

	/*tokens, err = refresh_tokens(tokens.refresh_token)
	if err == nil {
		fmt.Println(tokens.access_token)
		fmt.Println(tokens.refresh_token)
		print_tokens(tokens)
	} else {
		fmt.Println("login error")
	}
	fmt.Printf("\n\n")
	tokens, err = refresh_tokens(tokens.refresh_token)
	if err == nil {
		fmt.Println(tokens.access_token)
		fmt.Println(tokens.refresh_token)
		print_tokens(tokens)
	} else {
		fmt.Println("login error")
	}
	fmt.Println()
	fmt.Println(storage)
	err = logout(tokens2.refresh_token)
	fmt.Println(storage)
	err = force_logout_from_all_account_sessions("user")
	fmt.Println(storage)*/


	/*start1 := time.Now()
	var i = 0
	for i < 10000000{
		var buf bytes.Buffer
		buf.WriteString("1543075458.")
		buf.WriteString("299015.5")
		str := buf.String()
		err = fmt.Errorf(str)
		i++
	}
	end1 := time.Now()
	dur1 := end1.Sub(start1)
	log.Printf("concat took %s", dur1)

	start2 := time.Now()
	i = 0
	for i < 10000000{
		str := "1543075458.299015.5"
		err = fmt.Errorf(str)
		i++
	}
	end2 := time.Now()
	dur2 := end2.Sub(start2)
	log.Printf("base took %s", dur2)
	t1 := time.Now().Add(dur1)
	t2 := time.Now().Add(dur2)
	delta := t1.Sub(t2)
	log.Printf("\n\n")*/

	/*var naeb1 = Tokens{}
	naeb1.access_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJncm91cHNfaWQiOlsxLDMsMyw3XSwiZXhwIjoxNTQzMDc3MzcxLCJpYXQiOjE1NDMwNzcwNzF9.irheU73jorwN4efc6IqCJwGpiee3Il_UKBXX6iyWevE"
	naeb1.refresh_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJncm91cHNfaWQiOlsxLDMsMyw3XSwidXNlcl9pZCI6InVzZXIiLCJ0b2tlbl9pZCI6IjE1NDMwNzcwNzEuOTcxMzI3LjUiLCJpYXQiOjE1NDMwNzcwNzF9.gep2TkW1UtcmNtd5475kerKzvQQ12FpkU3fWIzqeMy8"

	fmt.Println(is_auth(naeb1.access_token))
	tokens, err = refresh_tokens(naeb1.refresh_token)
	if err == nil {
		fmt.Println(tokens.access_token)
		fmt.Println(tokens.refresh_token)
		print_tokens(tokens)
	} else {
		fmt.Println("palevo", err)
	}*/

	end2 := time.Now()
	dur2 := end2.Sub(start2)
	fmt.Println("base took %s", dur2)

}
